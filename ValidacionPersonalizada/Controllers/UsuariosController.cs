﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ValidacionPersonalizada.Models;

namespace ValidacionPersonalizada.Controllers
{
    public class UsuariosController : Controller
    {
        private Model db = new Model();

        // GET: Usuarios
        public ActionResult Index()
        {
            return View(db.USUARIOS_Nombre.ToList());
        }

        // GET: Usuarios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USUARIOS_Nombre uSUARIOS_Nombre = db.USUARIOS_Nombre.Find(id);
            if (uSUARIOS_Nombre == null)
            {
                return HttpNotFound();
            }
            return View(uSUARIOS_Nombre);
        }

        // GET: Usuarios/Create
        public ActionResult Create()
        {
            return View(new USUARIOS_Nombre());
        }

        // POST: Usuarios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(USUARIOS_Nombre uSUARIOS_Nombre)
        {
            if (ModelState.IsValid)
            {
                db.USUARIOS_Nombre.Add(uSUARIOS_Nombre);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(uSUARIOS_Nombre);
        }

        // GET: Usuarios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USUARIOS_Nombre uSUARIOS_Nombre = db.USUARIOS_Nombre.Find(id);
            if (uSUARIOS_Nombre == null)
            {
                return HttpNotFound();
            }
            return View(uSUARIOS_Nombre);
        }

        // POST: Usuarios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Usuario,Password,Correo,Genero,Activo")] USUARIOS_Nombre uSUARIOS_Nombre)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uSUARIOS_Nombre).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(uSUARIOS_Nombre);
        }

        // GET: Usuarios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USUARIOS_Nombre uSUARIOS_Nombre = db.USUARIOS_Nombre.Find(id);
            if (uSUARIOS_Nombre == null)
            {
                return HttpNotFound();
            }
            return View(uSUARIOS_Nombre);
        }

        // POST: Usuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            USUARIOS_Nombre uSUARIOS_Nombre = db.USUARIOS_Nombre.Find(id);
            db.USUARIOS_Nombre.Remove(uSUARIOS_Nombre);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
