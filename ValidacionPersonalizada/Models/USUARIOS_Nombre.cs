namespace ValidacionPersonalizada.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class USUARIOS_Nombre
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Usuario { get; set; }

        [Required]
        [StringLength(50)]
        public string Password { get; set; }

        [Required]
        [StringLength(100)]
        public string Correo { get; set; }

        

        public bool Activo { get; set; }

        [CustomValidator]
        [StringLength(10)]
        public string Genero { get; set; }
    }
}
