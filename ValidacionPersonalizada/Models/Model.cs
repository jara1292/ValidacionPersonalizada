namespace ValidacionPersonalizada.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model : DbContext
    {
        public Model()
            : base("name=Contexto")
        {
        }

        public virtual DbSet<USUARIOS_Nombre> USUARIOS_Nombre { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<USUARIOS_Nombre>()
                .Property(e => e.Usuario)
                .IsUnicode(false);

            modelBuilder.Entity<USUARIOS_Nombre>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<USUARIOS_Nombre>()
                .Property(e => e.Correo)
                .IsUnicode(false);

            modelBuilder.Entity<USUARIOS_Nombre>()
                .Property(e => e.Genero)
                .IsUnicode(false);
        }
    }
}
