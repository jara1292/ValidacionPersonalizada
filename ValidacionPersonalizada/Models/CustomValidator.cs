﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ValidacionPersonalizada.Models
{
    public class CustomValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var usuario = (USUARIOS_Nombre)validationContext.ObjectInstance;

            if (usuario.Activo)
            {
                if (value == null || string.IsNullOrEmpty(value.ToString()))
                {
                    // Oops!
                    return new ValidationResult(validationContext.DisplayName + " es requerido cuando el usuario esta activo.");
                }
            }

            // Ok!
            return ValidationResult.Success;
        }
    }
}